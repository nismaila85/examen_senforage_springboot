package sn.isi.service;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sn.isi.dto.AppClient;
import sn.isi.dto.AppCompteur;
import sn.isi.entities.AppCompteurEntity;
import sn.isi.entities.AppUserEntity;


@RunWith(SpringRunner.class)
@SpringBootTest
class AppCompteurServiceTest {

    @Autowired
    private AppCompteurService appCompteurService;

    @Test
    void getAppCompteurs() {
    }

    @Test
    void getAppCompteur() {
    }

    @Test
    void createAppCompteur() {

        AppCompteur appCompteur = new AppCompteur();
        appCompteur.setEtatCompteur(AppCompteurEntity.EtatCompteur.ON);
        appCompteur.setNumero("123456");
        appCompteur.getUser(AppUserEntity.Etat.GESCOM);
        appCompteur.setConsomation(1200.00);

        AppCompteur appCompteur1 = appCompteurService.createAppCompteur(appCompteur);
        Assertions.assertNotNull(appCompteur1);
    }

    @Test
    void updateAppCompteur() {
    }

    @Test
    void deleteAppCompteur() {
    }
}
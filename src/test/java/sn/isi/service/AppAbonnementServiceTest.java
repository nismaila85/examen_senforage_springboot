package sn.isi.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sn.isi.dto.AppAbonnement;
import sn.isi.dto.AppClient;
import sn.isi.entities.AppClientEntity;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class AppAbonnementServiceTest {

    @Autowired
    private AppAbonnementService appAbonnementService;

    @Test
    void getAbonnements() {

        List<AppAbonnement> abonnementList =
                appAbonnementService.getAbonnements();

        Assertions.assertEquals(2, abonnementList.size());


    }

    @Test
    void getAppAbonnement() {

        AppAbonnement appAbonnement = appAbonnementService.getAppAbonnement(2);
        Assertions.assertNotNull(appAbonnement);
    }

    @Test
    void createAppAbonnement() {
        AppAbonnement appAbonnement = new AppAbonnement();
        appAbonnement.setDate("2022-12-12");
        appAbonnement.setNumero("13333366");
        appAbonnement.setTexte("abonnement");
        AppAbonnement appAbonnement1 = appAbonnementService.createAppAbonnement(appAbonnement);
        Assertions.assertNotNull(appAbonnement1);
    }

    @Test
    void updateAppAbonnement() {

        AppAbonnement appAbonnement = new AppAbonnement();
        AppClient appCli = new AppClient();
        appAbonnement.setDate("2022-12-12");
        appAbonnement.setNumero("339543321");
        appAbonnement.setTexte("ismaila");
        AppAbonnement appAbonnementSave = appAbonnementService.updateAppAbonnement(1,appAbonnement);
        Assertions.assertEquals("339543321",appAbonnementSave.getNumero());
    }

    @Test
    void deleteAppAbonnement() {

        appAbonnementService.deleteAppAbonnement(2);
        Assertions.assertTrue(true);
    }
}
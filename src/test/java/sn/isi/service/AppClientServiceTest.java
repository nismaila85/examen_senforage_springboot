package sn.isi.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sn.isi.dto.AppAbonnement;
import sn.isi.dto.AppClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest
class AppClientServiceTest {

    @Autowired
    private AppClientService appClientService;

    @Test
    void getAppClients() {

        List<AppClient> appClients =
                appClientService.getAppClients();

        Assertions.assertEquals(1, appClients.size());


    }

    @Test
    void getAppClient() {

        AppClient appClient = appClientService.getAppClient(2);
        Assertions.assertNotNull(appClient);
    }

    @Test
    void createAppClient() {
        AppClient appClient = new AppClient();
        appClient.setNom("Sidy NDiaye");
        appClient.setAdresse("Amerique du sud");
        appClient.setNumero("+339232334");
        AppClient appClientSave = appClientService.createAppClient(appClient);
        Assertions.assertNotNull(appClientSave);
    }

    @Test
    void updateAppClient() {
    }

    @Test
    void deleteAppClient() {
    }
}
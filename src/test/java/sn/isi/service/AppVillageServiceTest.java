package sn.isi.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sn.isi.dto.AppAbonnement;
import sn.isi.dto.AppVillage;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class AppVillageServiceTest {

    @Autowired
    private AppVillageService appVillageService;
    @Test
    void getAppVillages() {
        List<AppVillage> appRolesList =
                appVillageService.getAppVillages();

        Assertions.assertEquals(2, appRolesList.size());


    }

    @Test
    void getAppVillage() {
        AppVillage appVillage = appVillageService.getAppVillage(2);
        Assertions.assertNotNull(appVillage);
    }

    @Test
    void createAppVillage() {

        AppVillage appVillage = new AppVillage();
        appVillage.setNom("Oumar");
        appVillage.setChef("Mr Sall");

        AppVillage appVillageSave = appVillageService.createAppVillage(appVillage);
        Assertions.assertNotNull(appVillageSave);
    }

    @Test
    void updateAppVillage() {

        AppVillage appVillage = new AppVillage();
        appVillage.setNom("Assane");
        appVillage.setChef("Mr Sall");
        AppVillage appVillageSave = appVillageService.updateAppVillage(2,appVillage);
        Assertions.assertEquals("Assane",appVillageSave.getNom());
    }

    @Test
    void deleteAppVillage() {

        appVillageService.deleteAppVillage(1);
        Assertions.assertTrue(true);
    }
}
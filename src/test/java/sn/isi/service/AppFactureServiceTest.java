package sn.isi.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sn.isi.dto.AppCompteur;
import sn.isi.dto.AppFacture;
import sn.isi.entities.AppCompteurEntity;
import sn.isi.entities.AppFactureEntity;
import sn.isi.entities.AppUserEntity;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class AppFactureServiceTest {

    @Autowired
    private AppFactureService appFactureService;
    @Test
    void getAppFactures() {
    }

    @Test
    void getAppFacture() {
    }

    @Test
    void createAppFacture() {

        AppFacture appFacture = new AppFacture();
        appFacture.setEtatFacture(AppFactureEntity.EtatFacture.non_réglée);
        appFacture.setConso(1299.00);
        appFacture.setPrix(235000);
        AppFacture appFacture1 = appFactureService.createAppFacture(appFacture);
        Assertions.assertNotNull(appFacture1);
    }

    @Test
    void updateAppFacture() {
    }

    @Test
    void deleteAppFacture() {
    }
}
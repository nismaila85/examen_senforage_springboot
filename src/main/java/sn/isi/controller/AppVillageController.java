package sn.isi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sn.isi.dto.AppVillage;
import sn.isi.service.AppVillageService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/villages")
@AllArgsConstructor
public class AppVillageController {
    private AppVillageService appVillageService;

    @GetMapping
    public List<AppVillage> getAppVillages() {
        return appVillageService.getAppVillages();
    }

    @GetMapping("/{id}")
    public AppVillage getAppVillages(@PathVariable("id") int id) {
        return appVillageService.getAppVillage(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AppVillage createAppRoles(@Valid @RequestBody AppVillage appVillage) {
        return appVillageService.createAppVillage(appVillage);
    }

    @PutMapping("/{id}")
    public AppVillage updateAppVillage(@PathVariable("id") int id, @Valid @RequestBody AppVillage appVillage) {
        return appVillageService.updateAppVillage(id, appVillage);
    }

    @DeleteMapping("/{id}")
    public void deleteAppRoles(@PathVariable("id") int id) {
        appVillageService.deleteAppVillage(id);
    }
}

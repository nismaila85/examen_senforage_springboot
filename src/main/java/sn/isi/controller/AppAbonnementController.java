package sn.isi.controller;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sn.isi.dto.AppAbonnement;
import sn.isi.service.AppAbonnementService;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/abonnements")
@AllArgsConstructor
public class AppAbonnementController {
    private AppAbonnementService appAbonnementService;

    @GetMapping
    public List<AppAbonnement> getAppAbonnements() {
        return appAbonnementService.getAbonnements();
    }

    @GetMapping("/{id}")
    public AppAbonnement getAppAbonnement(@PathVariable("id") int id) {
        return appAbonnementService.getAppAbonnement(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AppAbonnement createAppAbonnement(@Valid @RequestBody AppAbonnement appAbonnement) {
        return appAbonnementService.createAppAbonnement(appAbonnement);
    }

    @PutMapping("/{id}")
    public AppAbonnement updateAppAbonnement(@PathVariable("id") int id, @Valid @RequestBody AppAbonnement appAbonnement) {
        return appAbonnementService.updateAppAbonnement(id, appAbonnement);
    }

    @DeleteMapping("/{id}")
    public void deleteAppAbonnement(@PathVariable("id") int id) {
        appAbonnementService.deleteAppAbonnement(id);
    }
}

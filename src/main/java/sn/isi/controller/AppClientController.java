package sn.isi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sn.isi.dto.AppClient;
import sn.isi.service.AppClientService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/clients")
@AllArgsConstructor
public class AppClientController {
    private AppClientService appClientService;

    @GetMapping
    public List<AppClient> getAppClients() {
        return appClientService.getAppClients();
    }

    @GetMapping("/{id}")
    public AppClient getAppClient(@PathVariable("id") int id) {
        return appClientService.getAppClient(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AppClient createAppClient(@Valid @RequestBody AppClient appClient) {
        return appClientService.createAppClient(appClient);
    }

    @PutMapping("/{id}")
    public AppClient updateAppClient(@PathVariable("id") int id, @Valid @RequestBody AppClient appClient) {
        return appClientService.updateAppClient(id, appClient);
    }

    @DeleteMapping("/{id}")
    public void deleteAppClient(@PathVariable("id") int id) {
        appClientService.deleteAppClient(id);
    }
}

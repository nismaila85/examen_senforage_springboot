package sn.isi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sn.isi.dto.AppCompteur;
import sn.isi.service.AppCompteurService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/compteurs")
@AllArgsConstructor
public class AppCompteurController {
    private AppCompteurService appCompteurService;

    @GetMapping
    public List<AppCompteur> getAppCompteurs() {
        return appCompteurService.getAppCompteurs();
    }

    @GetMapping("/{id}")
    public AppCompteur getAppCompteur(@PathVariable("id") int id) {
        return appCompteurService.getAppCompteur(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AppCompteur createAppCompteur(@Valid @RequestBody AppCompteur appCompteur) {
        return appCompteurService.createAppCompteur(appCompteur);
    }

    @PutMapping("/{id}")
    public AppCompteur updateAppCompteur(@PathVariable("id") int id, @Valid @RequestBody AppCompteur appCompteur) {
        return appCompteurService.updateAppCompteur(id, appCompteur);
    }

    @DeleteMapping("/{id}")
    public void deleteAppCompteur(@PathVariable("id") int id) {
        appCompteurService.deleteAppCompteur(id);
    }
}

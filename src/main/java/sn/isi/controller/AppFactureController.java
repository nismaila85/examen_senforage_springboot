package sn.isi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sn.isi.dto.AppFacture;
import sn.isi.entities.AppFactureEntity;
import sn.isi.service.AppFactureService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/factures")
@AllArgsConstructor
public class AppFactureController {
    private AppFactureService appFactureService;

    @GetMapping
    public List<AppFacture> getAppFactures() {
        return appFactureService.getAppFactures();
    }

    @GetMapping("/{id}")
    public AppFacture getAppAppFacture(@PathVariable("id") int id) {
        return appFactureService.getAppFacture(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AppFacture createAppFacture(@Valid @RequestBody AppFacture appFacture) {
        return appFactureService.createAppFacture(appFacture);
    }

    @PutMapping("/{id}")
    public AppFacture updateAppFacture(@PathVariable("id") int id, @Valid @RequestBody AppFacture appFacture) {
        return appFactureService.updateAppFacture(id, appFacture);
    }

    @DeleteMapping("/{id}")
    public void deleteAppFacture(@PathVariable("id") int id) {
        appFactureService.deleteAppFacture(id);
    }


    public double prixTotal(AppFactureEntity fac) {

        return fac.getConso() + fac.getPrix();

    }

    public double prixFactureNonregle(AppFactureEntity fac) {


        double prix =  fac.getConso() + fac.getPrix();
        double prixtaxee = prix*0.05;
        return prix+prixtaxee;

    }
}

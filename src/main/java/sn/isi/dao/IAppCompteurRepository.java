package sn.isi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.isi.entities.AppCompteurEntity;

public interface IAppCompteurRepository extends JpaRepository<AppCompteurEntity, Integer> {
}

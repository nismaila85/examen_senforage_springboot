package sn.isi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.isi.entities.AppVillageEntity;

public interface IAppVillageRepository extends JpaRepository<AppVillageEntity,Integer> {
}

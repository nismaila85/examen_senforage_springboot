package sn.isi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.isi.entities.AppClientEntity;

public interface IAppClientRepository extends JpaRepository<AppClientEntity,Integer> {
}

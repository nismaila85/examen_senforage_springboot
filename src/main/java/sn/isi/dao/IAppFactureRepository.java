package sn.isi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.isi.entities.AppFactureEntity;

public interface IAppFactureRepository extends JpaRepository<AppFactureEntity,Integer> {


}

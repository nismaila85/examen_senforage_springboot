package sn.isi.mapping;

import org.mapstruct.Mapper;
import sn.isi.dto.AppAbonnement;
import sn.isi.dto.AppClient;
import sn.isi.entities.AppAbonnementEntity;
import sn.isi.entities.AppClientEntity;

@Mapper
public interface AppClientMapper {

    AppClient toAppClient(AppClientEntity appClientEntity);
    AppClientEntity fromAppClient(AppClient appClient);
}

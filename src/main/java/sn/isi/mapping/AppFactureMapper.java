package sn.isi.mapping;

import org.mapstruct.Mapper;
import sn.isi.dto.AppCompteur;
import sn.isi.dto.AppFacture;
import sn.isi.entities.AppCompteurEntity;
import sn.isi.entities.AppFactureEntity;

@Mapper
public interface AppFactureMapper {

    AppFacture toAppFacture(AppFactureEntity appFactureEntity);
    AppFactureEntity fromAppFacture(AppFacture appFacture);
}

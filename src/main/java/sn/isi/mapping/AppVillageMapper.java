package sn.isi.mapping;

import org.mapstruct.Mapper;
import sn.isi.dto.AppCompteur;
import sn.isi.dto.AppVillage;
import sn.isi.entities.AppCompteurEntity;
import sn.isi.entities.AppVillageEntity;

@Mapper
public interface AppVillageMapper {
    AppVillage toAppVillage(AppVillageEntity appVillageEntity);
    AppVillageEntity fromAppVillage(AppVillage appVillage);
}

package sn.isi.mapping;

import org.mapstruct.Mapper;
import sn.isi.dto.AppAbonnement;
import sn.isi.entities.AppAbonnementEntity;
@Mapper
public interface AppAbonnementMapper {

    AppAbonnement toAppAbonnement(AppAbonnementEntity appAbonnementEntity);
    AppAbonnementEntity fromAppAbonnement(AppAbonnement appAbonnement);
}

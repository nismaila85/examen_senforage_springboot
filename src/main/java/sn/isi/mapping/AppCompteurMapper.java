package sn.isi.mapping;

import org.mapstruct.Mapper;
import sn.isi.dto.AppClient;
import sn.isi.dto.AppCompteur;
import sn.isi.entities.AppClientEntity;
import sn.isi.entities.AppCompteurEntity;

@Mapper
public interface AppCompteurMapper {

    AppCompteur toAppCompteur(AppCompteurEntity appCompteurEntity);
    AppCompteurEntity fromAppCompteur(AppCompteur appCompteur);
}

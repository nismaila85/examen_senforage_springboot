package sn.isi.service;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.isi.dao.IAppAbonnementRepository;
import sn.isi.dto.AppAbonnement;
import sn.isi.exception.EntityNotFoundException;
import sn.isi.exception.RequestException;
import sn.isi.mapping.AppAbonnementMapper;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class AppAbonnementService {
    private IAppAbonnementRepository  iAppAbonnementRepository;
    private AppAbonnementMapper appAbonnementMapper;
    MessageSource messageSource;


    public AppAbonnementService(IAppAbonnementRepository iAppAbonnementRepository, AppAbonnementMapper appAbonnementMapper, MessageSource messageSource) {
        this.iAppAbonnementRepository = iAppAbonnementRepository;
        this.appAbonnementMapper = appAbonnementMapper;
        this.messageSource = messageSource;
    }

    @Transactional(readOnly = true)
    public List<AppAbonnement>  getAbonnements() {
        return StreamSupport.stream(iAppAbonnementRepository.findAll().spliterator(), false)
                .map(appAbonnementMapper::toAppAbonnement)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public AppAbonnement getAppAbonnement(int id) {
        return appAbonnementMapper.toAppAbonnement(iAppAbonnementRepository.findById(id)
                .orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage("abonnement.notfound", new Object[]{id},
                        Locale.getDefault()))));
    }

    @Transactional
    public AppAbonnement createAppAbonnement(AppAbonnement appAbonnement) {
        return appAbonnementMapper.toAppAbonnement(iAppAbonnementRepository.save(appAbonnementMapper.fromAppAbonnement(appAbonnement)));
    }


    @Transactional
    public AppAbonnement updateAppAbonnement(int id,  AppAbonnement appAbonnement) {
        return iAppAbonnementRepository.findById(id)
                .map(entity -> {
                    appAbonnement.setId(id);
                    return appAbonnementMapper.toAppAbonnement(
                            iAppAbonnementRepository.save(appAbonnementMapper.fromAppAbonnement(appAbonnement)));
                }).orElseThrow(() -> new EntityNotFoundException(messageSource.getMessage("abonnement.notfound", new Object[]{id},
                        Locale.getDefault())));
    }

    @Transactional
    public void deleteAppAbonnement(int id) {
        try {
            iAppAbonnementRepository.deleteById(id);
        } catch (Exception e) {
            throw new RequestException(messageSource.getMessage("abonnement.errordeletion", new Object[]{id},
                    Locale.getDefault()),
                    HttpStatus.CONFLICT);
        }
    }
}

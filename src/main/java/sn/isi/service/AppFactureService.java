package sn.isi.service;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.isi.dao.IAppFactureRepository;
import sn.isi.dao.IAppVillageRepository;
import sn.isi.dto.AppFacture;
import sn.isi.dto.AppVillage;
import sn.isi.exception.EntityNotFoundException;
import sn.isi.exception.RequestException;
import sn.isi.mapping.AppFactureMapper;
import sn.isi.mapping.AppVillageMapper;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class AppFactureService {
    private IAppFactureRepository iAppFactureRepository;
    private AppFactureMapper appFactureMapper;
    MessageSource messageSource;



    public AppFactureService(IAppFactureRepository iAppFactureRepository, AppFactureMapper appFactureMapper, MessageSource messageSource) {
        this.iAppFactureRepository = iAppFactureRepository;
        this.appFactureMapper = appFactureMapper;
        this.messageSource = messageSource;
    }

    @Transactional(readOnly = true)
    public List<AppFacture>  getAppFactures() {
        return StreamSupport.stream(iAppFactureRepository.findAll().spliterator(), false)
                .map(appFactureMapper::toAppFacture)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public AppFacture getAppFacture(int id) {
        return appFactureMapper.toAppFacture(iAppFactureRepository.findById(id)
                .orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage("facture.notfound", new Object[]{id},
                        Locale.getDefault()))));
    }

    @Transactional
    public AppFacture createAppFacture(AppFacture appFacture) {
        return appFactureMapper.toAppFacture(iAppFactureRepository.save(appFactureMapper.fromAppFacture( appFacture)));
    }

    @Transactional
    public AppFacture updateAppFacture(int id,  AppFacture appFacture) {
        return iAppFactureRepository.findById(id)
                .map(entity -> {
                    appFacture.setId(id);
                    return appFactureMapper.toAppFacture(
                            iAppFactureRepository.save(appFactureMapper.fromAppFacture(appFacture)));
                }).orElseThrow(() -> new EntityNotFoundException(messageSource.getMessage("facture.notfound", new Object[]{id},
                        Locale.getDefault())));
    }

    @Transactional
    public void deleteAppFacture(int id) {
        try {
            iAppFactureRepository.deleteById(id);
        } catch (Exception e) {
            throw new RequestException(messageSource.getMessage("facture.errordeletion", new Object[]{id},
                    Locale.getDefault()),
                    HttpStatus.CONFLICT);
        }
    }
}

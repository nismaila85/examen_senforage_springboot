package sn.isi.service;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.isi.dao.IAppClientRepository;
import sn.isi.dao.IAppVillageRepository;
import sn.isi.dto.AppClient;
import sn.isi.dto.AppVillage;
import sn.isi.exception.EntityNotFoundException;
import sn.isi.exception.RequestException;
import sn.isi.mapping.AppClientMapper;
import sn.isi.mapping.AppVillageMapper;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class AppClientService {
    private IAppClientRepository iAppClientRepository;
    private AppClientMapper appClientMapper;
    MessageSource messageSource;



    public AppClientService(IAppClientRepository iAppClientRepository, AppClientMapper appClientMapper, MessageSource messageSource) {
        this.iAppClientRepository = iAppClientRepository;
        this.appClientMapper = appClientMapper;
        this.messageSource = messageSource;
    }

    @Transactional(readOnly = true)
    public List<AppClient>  getAppClients() {
        return StreamSupport.stream(iAppClientRepository.findAll().spliterator(), false)
                .map(appClientMapper::toAppClient)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public AppClient getAppClient(int id) {
        return appClientMapper.toAppClient(iAppClientRepository.findById(id)
                .orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage("client.notfound", new Object[]{id},
                        Locale.getDefault()))));
    }

    @Transactional
    public AppClient createAppClient(AppClient appClient) {
        return appClientMapper.toAppClient(iAppClientRepository.save(appClientMapper.fromAppClient(appClient)));
    }

    @Transactional
    public AppClient updateAppClient(int id,  AppClient appClient) {
        return iAppClientRepository.findById(id)
                .map(entity -> {
                    appClient.setId(id);
                    return appClientMapper.toAppClient(
                            iAppClientRepository.save(appClientMapper.fromAppClient(appClient)));
                }).orElseThrow(() -> new EntityNotFoundException(messageSource.getMessage("client.notfound", new Object[]{id},
                        Locale.getDefault())));
    }

    @Transactional
    public void deleteAppClient(int id) {
        try {
            iAppClientRepository.deleteById(id);
        } catch (Exception e) {
            throw new RequestException(messageSource.getMessage("client.errordeletion", new Object[]{id},
                    Locale.getDefault()),
                    HttpStatus.CONFLICT);
        }
    }
}

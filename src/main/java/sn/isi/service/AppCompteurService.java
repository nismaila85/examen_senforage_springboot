package sn.isi.service;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.isi.dao.IAppCompteurRepository;
import sn.isi.dao.IAppVillageRepository;
import sn.isi.dto.AppCompteur;
import sn.isi.dto.AppVillage;
import sn.isi.exception.EntityNotFoundException;
import sn.isi.exception.RequestException;
import sn.isi.mapping.AppCompteurMapper;
import sn.isi.mapping.AppVillageMapper;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class AppCompteurService {
    private IAppCompteurRepository iAppCompteurRepository;
    private AppCompteurMapper appCompteurMapper;
    MessageSource messageSource;


    public AppCompteurService(IAppCompteurRepository iAppCompteurRepository, AppCompteurMapper appCompteurMapper, MessageSource messageSource) {
        this.iAppCompteurRepository = iAppCompteurRepository;
        this.appCompteurMapper = appCompteurMapper;
        this.messageSource = messageSource;
    }

    @Transactional(readOnly = true)
    public List<AppCompteur>  getAppCompteurs() {
        return StreamSupport.stream(iAppCompteurRepository.findAll().spliterator(), false)
                .map(appCompteurMapper::toAppCompteur)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public AppCompteur getAppCompteur(int id) {
        return appCompteurMapper.toAppCompteur(iAppCompteurRepository.findById(id)
                .orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage("compteur.notfound", new Object[]{id},
                        Locale.getDefault()))));
    }

    @Transactional
    public AppCompteur createAppCompteur(AppCompteur appCompteur) {
        return appCompteurMapper.toAppCompteur(iAppCompteurRepository.save(appCompteurMapper.fromAppCompteur(appCompteur)));
    }

    @Transactional
    public AppCompteur updateAppCompteur(int id,  AppCompteur appCompteur) {
        return iAppCompteurRepository.findById(id)
                .map(entity -> {
                    appCompteur.setId(id);
                    return appCompteurMapper.toAppCompteur(
                            iAppCompteurRepository.save(appCompteurMapper.fromAppCompteur(appCompteur)));
                }).orElseThrow(() -> new EntityNotFoundException(messageSource.getMessage("compteur.notfound", new Object[]{id},
                        Locale.getDefault())));
    }

    @Transactional
    public void deleteAppCompteur(int id) {
        try {
            iAppCompteurRepository.deleteById(id);
        } catch (Exception e) {
            throw new RequestException(messageSource.getMessage("compteur.errordeletion", new Object[]{id},
                    Locale.getDefault()),
                    HttpStatus.CONFLICT);
        }
    }
}

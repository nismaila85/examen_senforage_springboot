package sn.isi.service;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.isi.dao.IAppVillageRepository;
import sn.isi.dto.AppVillage;
import sn.isi.exception.EntityNotFoundException;
import sn.isi.exception.RequestException;
import sn.isi.mapping.AppVillageMapper;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class AppVillageService {
    private IAppVillageRepository iAppVillageRepository;
    private AppVillageMapper appVillageMapper;
    MessageSource messageSource;


    public AppVillageService(IAppVillageRepository iAppVillageRepository, AppVillageMapper appVillageMapper, MessageSource messageSource) {
        this.iAppVillageRepository = iAppVillageRepository;
        this.appVillageMapper = appVillageMapper;
        this.messageSource = messageSource;
    }

    @Transactional(readOnly = true)
    public List<AppVillage>  getAppVillages() {
        return StreamSupport.stream(iAppVillageRepository.findAll().spliterator(), false)
                .map(appVillageMapper::toAppVillage)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public AppVillage getAppVillage(int id) {
        return appVillageMapper.toAppVillage(iAppVillageRepository.findById(id)
                .orElseThrow(() ->
                new EntityNotFoundException(messageSource.getMessage("village.notfound", new Object[]{id},
                        Locale.getDefault()))));
    }

    @Transactional
    public AppVillage createAppVillage(AppVillage appVillage) {
        return appVillageMapper.toAppVillage(iAppVillageRepository.save(appVillageMapper.fromAppVillage(appVillage)));
    }

    @Transactional
    public AppVillage updateAppVillage(int id,  AppVillage appVillage) {
        return iAppVillageRepository.findById(id)
                .map(entity -> {
                    appVillage.setId(id);
                    return appVillageMapper.toAppVillage(
                            iAppVillageRepository.save(appVillageMapper.fromAppVillage(appVillage)));
                }).orElseThrow(() -> new EntityNotFoundException(messageSource.getMessage("village.notfound", new Object[]{id},
                        Locale.getDefault())));
    }

    @Transactional
    public void deleteAppVillage(int id) {
        try {
            iAppVillageRepository.deleteById(id);
        } catch (Exception e) {
            throw new RequestException(messageSource.getMessage("village.errordeletion", new Object[]{id},
                    Locale.getDefault()),
                    HttpStatus.CONFLICT);
        }
    }
}

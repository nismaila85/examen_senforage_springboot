package sn.isi.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppFactureEntity {

    public enum EtatFacture{
        non_réglée,
        réglée

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private double conso;
    @Column(length = 100, nullable = false)
    private double prix;
    private EtatFacture etatFacture;
    @ManyToOne
    private AppClientEntity client;
    @ManyToOne
    private AppUserEntity user;
}

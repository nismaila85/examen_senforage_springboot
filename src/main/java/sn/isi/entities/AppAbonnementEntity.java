package sn.isi.entities;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppAbonnementEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private String date;
    @Column(length = 100, nullable = true)
    private String numero;
    @Column(length = 100, nullable = false)
    private String texte;
    @OneToOne
    private AppClientEntity client;
    @ManyToOne
    private AppUserEntity user;


}

package sn.isi.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUserEntity {

    public enum Etat {
        ADMIN,
        GESCOM,
        GESCLI,
        GESCOMP;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private String nom;
    @Column(length = 100, nullable = false)
    private String prenom;
    @Column(length = 100, nullable = false)
    private String email;
    @Column(length = 100, nullable = false)
    private String password;
    @Column(length = 100, nullable = false)
    private Etat etat;
    @OneToMany(mappedBy = "user")
    List<AppFactureEntity> factures = new ArrayList<AppFactureEntity>();
    @OneToMany(mappedBy = "user")
    List<AppAbonnementEntity> abonnements = new ArrayList<AppAbonnementEntity>();
    @OneToMany(mappedBy = "user")
    List<AppCompteurEntity> compteurs = new ArrayList<AppCompteurEntity>();
    @OneToMany(mappedBy = "user")
    List<AppClientEntity> clients = new ArrayList<AppClientEntity>();
}

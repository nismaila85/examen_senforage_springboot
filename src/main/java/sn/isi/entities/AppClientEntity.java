package sn.isi.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private String nom;
    @Column(length = 100, nullable = false)
    private String adresse ;
    @Column(length = 100, nullable = false)
    private String numero;
    @ManyToOne
    private AppVillageEntity village;
    @OneToMany(mappedBy = "client")
    List<AppFactureEntity> factures = new ArrayList<AppFactureEntity>();
    @OneToOne
    private AppAbonnementEntity abonnement;
    @ManyToOne
    private AppUserEntity user;
    @OneToOne
    private AppCompteurEntity compteur;
}

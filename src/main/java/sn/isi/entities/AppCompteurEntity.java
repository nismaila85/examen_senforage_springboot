package sn.isi.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppCompteurEntity {

    public enum EtatCompteur{
        OFF,
        ON
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private String numero;
    @Column(length = 100, nullable = false)
    private double consomation;
    private EtatCompteur etatCompteur;
    @ManyToOne
    private AppUserEntity user;
    @OneToOne
    private AppAbonnementEntity client;
}

package sn.isi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProSenforageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProSenforageApplication.class, args);
	}

}

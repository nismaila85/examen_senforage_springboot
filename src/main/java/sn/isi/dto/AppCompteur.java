package sn.isi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.isi.entities.AppAbonnementEntity;
import sn.isi.entities.AppCompteurEntity;
import sn.isi.entities.AppUserEntity;

import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppCompteur {
    private int id;
    @NotNull(message = "Le numero ne doit pas etre null")
    private String numero;
    @NotNull(message = "La consommation ne doit pas etre null")
    private double consomation;
    @NotNull(message="L'etat ne doit pas etre null")
    private AppCompteurEntity.EtatCompteur etatCompteur;
    @ManyToOne
    private AppUserEntity user;
    @OneToOne
    private AppAbonnementEntity client;

    public AppUserEntity getUser(AppUserEntity.Etat GESCOM) {
        return user;
    }
}

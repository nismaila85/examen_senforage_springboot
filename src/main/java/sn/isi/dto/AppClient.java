package sn.isi.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.isi.entities.*;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppClient {
    private int id;
    @NotNull(message="Le nom ne doit pas etre null")
    private String nom;
    @NotNull(message="L'adresse ne doit pas etre null")
    private String adresse ;
    @NotNull(message="Le numero ne doit pas etre null")
    private String numero;
    @ManyToOne
    private AppVillageEntity village;
    @OneToMany(mappedBy = "client")
    List<AppFactureEntity> factures = new ArrayList<AppFactureEntity>();
    @OneToOne
    private AppAbonnementEntity abonnement;
    @ManyToOne
    private AppUserEntity user;
    @OneToOne
    private AppCompteurEntity compteur;


    public AppAbonnementEntity getAbonnement() {
        return abonnement;
    }

    public void setAbonnement(AppAbonnementEntity abonnement) {
        this.abonnement = abonnement;
    }
}

package sn.isi.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.isi.entities.AppFactureEntity;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppFacture {

    private int id;
    @NotNull(message="La conso ne doit pas etre null")
    private double conso;
    @NotNull(message="Le prix ne doit pas etre null")
    private double prix;
    @NotNull(message="L'etat ne doit pas etre null")
    private AppFactureEntity.EtatFacture etatFacture;
}

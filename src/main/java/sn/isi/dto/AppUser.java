package sn.isi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.isi.entities.AppUserEntity;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUser {

    private int id;
    @NotNull(message="Le nom ne doit pas etre null")
    private String nom;
    @NotNull(message="Le prenom ne doit pas etre null")
    private String prenom;
    @NotNull(message="L'email ne doit pas etre null")
    private String email;
    @NotNull(message="Le password ne doit pas etre null")
    private String password;
    @NotNull(message="L'etat conso ne doit pas etre null")
    private AppUserEntity.Etat etat;
}

package sn.isi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.isi.entities.AppClientEntity;
import sn.isi.entities.AppUserEntity;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppAbonnement {

    private int id;
    @NotNull(message="La date ne doit pas etre null")
    private String date;
    @NotNull(message="Le numero ne doit pas etre null")
    private String numero;
    @NotNull(message="Le texte ne doit pas etre null")
    private String texte;
    private AppClientEntity client;
    private AppUserEntity user;

    public AppClientEntity getClient(AppClientEntity client) {
        return client;
    }


}
